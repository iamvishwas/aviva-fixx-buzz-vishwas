package com.controller;

import java.util.List;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.NumberModel;
import com.service.NumberService;

@RestController
@RequestMapping("/aviva_buzz")
public class TestController {

@Autowired
NumberService numberService;

private static List<Integer> numberList = NumberModel.getNumberList();

	@RequestMapping(value = "/numbers/{num}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, //
                    MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public List<Integer> addNumbers(@PathVariable("num") @Range(min = 0l,max=1000, message = "Please select positive numbers Only") int num) {
		
		List numberList = numberService.addNumbers(num);
        return numberList;
    }
	
	@RequestMapping(value="/allnumbers/{start}/{size}",  method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public List<Integer> getAllNumbers(@PathVariable("start") int start,@PathVariable("size") int size) {
		List numberList = numberService.getAllNumbers(start, size);
		return numberList;
    }
	
}
