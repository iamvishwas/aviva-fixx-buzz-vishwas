package com.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.model.NumberModel;
import com.service.NumberService;

@Component
public class NumberServiceImpl implements NumberService{

	private static List<Integer> numberList = NumberModel.getNumberList();
	
	@Override
	public List<Integer> addNumbers(int num) {

		if(num%3==0){
			System.out.println("fizz");
		}
		if(num%5==0){
			System.out.println("buzz");
		}
		if(num%3==0 && num%5==0){
			System.out.println("fizz buzz");
		}
        numberList.add(num);
        
		return numberList;
	}

	@Override
	public List<Integer> getAllNumbers(int start, int size) {
		if(start >= 0 && size > 0) {
			return getPaginatedCustomer(start, size);			
		}
		
		return new ArrayList<>(numberList);
		
	}

	private List<Integer> getPaginatedCustomer(int start, int size) {
		if(start+size > numberList.size() ) {
			return new ArrayList<>(numberList).subList(start, numberList.size());
		}else {
			return new ArrayList<>(numberList).subList(start, size + start);
		}
	}
}
