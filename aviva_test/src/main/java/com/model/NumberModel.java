package com.model;

import java.util.ArrayList;
import java.util.List;

public class NumberModel {
int num;
private static List<Integer> numberList = initList();

private static List<Integer> initList(){
	List<Integer> list = new ArrayList<Integer>();
	list.add(1);
	list.add(2);
	return list;
	
}

public static List<Integer> getNumberList(){
	return numberList;
}
public int getNum() {
	return num;
}

public void setNum(int num) {
	this.num = num;
}

}
