package com.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public interface NumberService {

	 public List<Integer> addNumbers(int num);
	 public List<Integer> getAllNumbers(int start,int size);
}
