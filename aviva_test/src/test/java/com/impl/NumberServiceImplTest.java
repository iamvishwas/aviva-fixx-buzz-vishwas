package com.impl;

import java.util.List;

import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

import junit.framework.Assert;

@RunWith(JUnit4ClassRunner.class)
public class NumberServiceImplTest {

	@Test
	public void testAddNumber(){
		
		NumberServiceImpl numberServiceImpl = new NumberServiceImpl();
		List<Integer> numberlist = numberServiceImpl.addNumbers(19);
		Assert.assertEquals(19, numberlist.contains(19));
	}
	
	@Test
	public void getAllNumbers(){
		
		NumberServiceImpl numberServiceImpl = new NumberServiceImpl();
		numberServiceImpl.getAllNumbers(1, 20);
		
	}
}
